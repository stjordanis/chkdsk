# CHKDSK

Check the disk for errors.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## CHKDSK.LSM

<table>
<tr><td>title</td><td>CHKDSK</td></tr>
<tr><td>version</td><td>beta 0.9.2 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-06-13</td></tr>
<tr><td>description</td><td>Check the disk for errors.</td></tr>
<tr><td>keywords</td><td>Check disk errors</td></tr>
<tr><td>author</td><td>Imre Leber &lt;imre.leber -AT- telenet.be&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Imre Leber &lt;imre.leber -AT- telenet.be&gt;</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Chkdsk</td></tr>
</table>
